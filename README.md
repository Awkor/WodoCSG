<h1 align="center">WodoCSG</h1>

<p align="center">A 3D preview generator for operations on wood panels.</p>

## Building

As a first step, you'll need to clone the repository to your machine:

```
git clone https://github.com/Awkor/WodoCSG.git
```

### Linux

After having cloned the repository, go inside its folder:

```
cd WodoCSG
```

Then, use `cmake` to generate the needed makefiles:

```
cmake .
```

And after `cmake` is done, use `make` to build the binaries.

```
make
```

If everything went well, a `binaries` folder should have been created with the main executable inside it.

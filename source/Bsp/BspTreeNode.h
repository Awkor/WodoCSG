#pragma once

#include <memory>
#include <vector>

#include <Geometry/Geometry.h>
#include <Geometry/Plane.h>
#include <Geometry/Triangle.h>

class BspTreeNode {
public:
  std::unique_ptr<BspTreeNode> back;
  std::unique_ptr<BspTreeNode> front;
  Plane plane;
  BspTreeNode(std::vector<Triangle> &triangles);
  auto clip(BspTreeNode &node) -> void;
  auto clipped(std::vector<Triangle> &triangles) -> std::vector<Triangle>;
  auto invert() -> void;
  auto triangles() -> std::vector<Triangle>;

private:
  std::vector<Triangle> _triangles;
};

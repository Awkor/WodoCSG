#include <Bsp/BspTreeNode.h>

BspTreeNode::BspTreeNode(std::vector<Triangle> &triangles) {
  // Use the first triangle to create a plane.
  Triangle triangle = triangles.front();
  plane = Plane(triangle);
  // Given the current node's plane, use it to split the passed triangles.
  std::vector<Triangle> backTriangles;
  std::vector<Triangle> coplanarTriangles;
  std::vector<Triangle> frontTriangles;
  for (Triangle &triangle : triangles) {
    Geometry::split(plane, triangle, backTriangles, coplanarTriangles,
                    coplanarTriangles, frontTriangles);
  }
  // Store coplanar triangles in the current node.
  _triangles = coplanarTriangles;
  // Create new nodes depending on where triangles remain.
  if (backTriangles.size() > 0) {
    back = std::make_unique<BspTreeNode>(backTriangles);
  }
  if (frontTriangles.size() > 0) {
    front = std::make_unique<BspTreeNode>(frontTriangles);
  }
}

auto BspTreeNode::clip(BspTreeNode &node) -> void {
  _triangles = node.clipped(_triangles);
  if (front) {
    front->clip(node);
  }
  if (back) {
    back->clip(node);
  }
}

auto BspTreeNode::clipped(std::vector<Triangle> &triangles)
    -> std::vector<Triangle> {

  std::vector<Triangle> backTriangles;
  std::vector<Triangle> frontTriangles;

  for (Triangle &triangle : triangles) {
    Geometry::split(plane, triangle, backTriangles, backTriangles,
                    frontTriangles, frontTriangles);
  }

  if (front) {
    frontTriangles = front->clipped(frontTriangles);
  }

  if (back) {
    backTriangles = back->clipped(backTriangles);
  } else {
    backTriangles.clear();
  }

  frontTriangles.insert(frontTriangles.end(), backTriangles.begin(),
                        backTriangles.end());

  return frontTriangles;
}

auto BspTreeNode::invert() -> void {
  _triangles = Geometry::flip(_triangles);
  // Update node's plane.
  Triangle triangle = _triangles.at(0);
  plane = Plane(triangle);
  // Invert children.
  if (front) {
    front->invert();
  }
  if (back) {
    back->invert();
  }
  front.swap(back);
}

auto BspTreeNode::triangles() -> std::vector<Triangle> {
  std::vector<Triangle> triangles = _triangles;
  if (front) {
    std::vector<Triangle> frontTriangles;
    frontTriangles = front->triangles();
    triangles.insert(triangles.end(), frontTriangles.begin(),
                     frontTriangles.end());
  }
  if (back) {
    std::vector<Triangle> backTriangles;
    backTriangles = back->triangles();
    triangles.insert(triangles.end(), backTriangles.begin(),
                     backTriangles.end());
  }
  return triangles;
}

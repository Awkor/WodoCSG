#include <Csg/Csg.h>

auto Csg::merge(BspTreeNode &a, BspTreeNode &b) -> std::vector<Triangle> {
  a.clip(b);
  b.clip(a);
  std::vector<Triangle> result;
  std::vector<Triangle> trianglesA = a.triangles();
  std::vector<Triangle> trianglesB = b.triangles();
  result.insert(result.begin(), trianglesA.begin(), trianglesA.end());
  result.insert(result.begin(), trianglesB.begin(), trianglesB.end());
  return result;
}

auto Csg::merge(std::vector<Triangle> &a, std::vector<Triangle> &b)
    -> std::vector<Triangle> {
  BspTreeNode c(a);
  BspTreeNode d(b);
  return Csg::merge(c, d);
}

auto Csg::subtract(BspTreeNode &a, BspTreeNode &b) -> std::vector<Triangle> {
  a.invert();
  a.clip(b);
  b.clip(a);
  std::vector<Triangle> result;
  std::vector<Triangle> trianglesA = a.triangles();
  trianglesA = Geometry::flip(trianglesA);
  std::vector<Triangle> trianglesB = b.triangles();
  trianglesB = Geometry::flip(trianglesB);
  result.insert(result.begin(), trianglesA.begin(), trianglesA.end());
  result.insert(result.begin(), trianglesB.begin(), trianglesB.end());
  return result;
}

auto Csg::subtract(std::vector<Triangle> &a, std::vector<Triangle> &b)
    -> std::vector<Triangle> {
  BspTreeNode c(a);
  BspTreeNode d(b);
  return Csg::subtract(c, d);
}

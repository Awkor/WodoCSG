#pragma once

#include <vector>

#include <Bsp/BspTreeNode.h>
#include <Geometry/Triangle.h>

namespace Csg {
auto merge(BspTreeNode &a, BspTreeNode &b) -> std::vector<Triangle>;
auto merge(std::vector<Triangle> &a, std::vector<Triangle> &b)
    -> std::vector<Triangle>;
auto subtract(BspTreeNode &a, BspTreeNode &b) -> std::vector<Triangle>;
auto subtract(std::vector<Triangle> &a, std::vector<Triangle> &b)
    -> std::vector<Triangle>;
} // namespace Csg

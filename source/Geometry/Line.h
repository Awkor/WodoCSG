#pragma once

#include <vector>

#include <Geometry/Vertex.h>
#include <Magnum/Magnum.h>
#include <Magnum/Math/Vector3.h>

using namespace Magnum;

struct Line {
  std::vector<Vertex> vertices;
  Line(const Vertex a, const Vertex b);
  auto direction(const bool inverted = false) const -> Vector3;
};

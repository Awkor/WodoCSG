#include <Geometry/Vertex.h>

Vertex::Vertex() = default;

Vertex::Vertex(const Vector3 position) : position(position) {}

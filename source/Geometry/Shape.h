#pragma once

#include <vector>

#include <Geometry/Geometry.h>
#include <Geometry/Triangle.h>
#include <Geometry/Vertex.h>
#include <Magnum/Magnum.h>
#include <Magnum/Math/Functions.h>
#include <Magnum/Math/Math.h>
#include <Magnum/Math/Vector3.h>
#include <Magnum/Types.h>

using namespace Magnum;
using namespace Magnum::Math::Literals;

namespace Shape {
auto arc(Vector3 a, Vector3 b, Float bulge, Float width, Float depth,
         UnsignedInt segmentCount) -> std::vector<Triangle>;
auto circle(Deg angle, Float radius, UnsignedInt sideCount)
    -> std::vector<Triangle>;
auto cuboid(Float width, Float height, Float depth) -> std::vector<Triangle>;
auto cuboid(Vector3 dimensions) -> std::vector<Triangle>;
auto cylinder(Deg angle, Float radius, Float height, UnsignedInt sideCount)
    -> std::vector<Triangle>;
auto rectangle(Vector3 left_bottom, Vector3 left_top, Vector3 right_bottom,
               Vector3 right_top) -> std::vector<Triangle>;
auto rectangle(Float width, Float height) -> std::vector<Triangle>;
} // namespace Shape

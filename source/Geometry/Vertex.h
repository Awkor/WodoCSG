#pragma once

#include <Magnum/Magnum.h>
#include <Magnum/Math/Vector3.h>

using namespace Magnum;

struct Vertex {
  Vector3 normal;
  Vector3 position;
  Vertex();
  Vertex(const Vector3 position);
};

#pragma once

#include <Geometry/Triangle.h>
#include <Geometry/Vertex.h>
#include <Magnum/Magnum.h>
#include <Magnum/Math/Complex.h>
#include <Magnum/Math/Vector3.h>
#include <Magnum/Types.h>

using namespace Magnum;

struct Plane {
  Float distance;
  Vector3 normal;
  Plane();
  Plane(const Float distance, const Vector3 normal);
  Plane(const Triangle &triangle);
};

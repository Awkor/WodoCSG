#include <Geometry/Geometry.h>

auto Geometry::flip(Triangle triangle) -> Triangle {
  Triangle flippedTriangle(triangle);
  std::reverse(flippedTriangle.vertices.begin(),
               flippedTriangle.vertices.end());
  for (Vertex &vertex : flippedTriangle.vertices) {
    vertex.normal = -vertex.normal;
  }
  return flippedTriangle;
}

auto Geometry::flip(std::vector<Triangle> triangles) -> std::vector<Triangle> {
  for (Triangle &triangle : triangles) {
    triangle = Geometry::flip(triangle);
  }
  return triangles;
}

auto Geometry::intersect(const Line &line, const Plane &plane) -> Vector3 {
  Vertex lineVertex = line.vertices.front();
  Vector3 planePosition = plane.normal * plane.distance;
  Vector3 delta = lineVertex.position - planePosition;
  Float dividend = Math::dot(delta, plane.normal); // TODO: Rename.
  Vector3 lineDirection = line.direction();
  Float divisor = Math::dot(lineDirection, plane.normal); // TODO: Rename.
  Float ratio = dividend / divisor;                       // TODO: Rename.
  return lineVertex.position - lineDirection * ratio;
}

auto Geometry::rotate(Triangle triangle, Deg angle, Vector3 axis) -> Triangle {
  Matrix4 rotationMatrix = Matrix4(1.0f);
  rotationMatrix = rotationMatrix.rotation(angle, axis);
  for (Vertex &vertex : triangle.vertices) {
    vertex.position = (rotationMatrix * Vector4(vertex.position, 1.0f)).xyz();
  }
  for (Vertex &vertex : triangle.vertices) {
    vertex.normal = triangle.normal();
  }
  return triangle;
}

auto Geometry::rotate(std::vector<Triangle> triangles, Deg angle, Vector3 axis)
    -> std::vector<Triangle> {
  std::vector<Triangle> rotatedTriangles;
  for (Triangle triangle : triangles) {
    Triangle rotatedTriangle = Geometry::rotate(triangle, angle, axis);
    rotatedTriangles.push_back(rotatedTriangle);
  }
  return rotatedTriangles;
}

void Geometry::split(Plane &plane, Triangle &triangle,
                     std::vector<Triangle> &back,
                     std::vector<Triangle> &coplanarBack,
                     std::vector<Triangle> &coplanarFront,
                     std::vector<Triangle> &front) {
  std::vector<Vertex> backVertices;
  std::vector<Vertex> coplanarBackVertices;
  std::vector<Vertex> coplanarFrontVertices;
  std::vector<Vertex> frontVertices;

  enum class VertexPosition { BACK, COPLANAR, FRONT };

  Containers::StaticArray<3, VertexPosition> vertexTypes;

  UnsignedInt backVertexCount = 0;
  UnsignedInt coplanarVertexCount = 0;
  UnsignedInt frontVertexCount = 0;

  Vector3 planePosition = plane.normal * plane.distance;
  for (UnsignedInt index = 0; index < triangle.vertices.size(); index++) {
    Vertex vertex = triangle.vertices[index];
    Vector3 delta = vertex.position - planePosition;
    Vector3 direction = delta.normalized();
    Float dot = Math::dot(direction, plane.normal);
    if (dot > 0.0001f) {
      vertexTypes[index] = VertexPosition::FRONT;
      frontVertexCount++;
    } else {
      if (dot < -0.0001f) {
        vertexTypes[index] = VertexPosition::BACK;
        backVertexCount++;
      } else {
        vertexTypes[index] = VertexPosition::COPLANAR;
        coplanarVertexCount++;
      }
    }
  }

  if (backVertexCount == 3) {
    back.emplace_back(triangle);
    return;
  }

  if (coplanarVertexCount == 3) {
    Vertex vertex = triangle.vertices.front();
    Float dot = Math::dot(vertex.normal, plane.normal);
    if (dot > 0.00001f) {
      coplanarFront.emplace_back(triangle);
    } else {
      coplanarBack.emplace_back(triangle);
    }
    return;
  }

  if (frontVertexCount == 3) {
    front.emplace_back(triangle);
    return;
  }

  if (coplanarVertexCount == 2) {
    for (UnsignedInt index = 0; index < triangle.vertices.size(); index++) {
      if (vertexTypes[index] != VertexPosition::COPLANAR) {
        if (vertexTypes[index] == VertexPosition::BACK) {
          back.emplace_back(triangle);
        } else {
          front.emplace_back(triangle);
        }
        return;
      }
    }
  }

  if (backVertexCount == 1 && frontVertexCount == 1) {
    for (UnsignedInt index = 0; index < triangle.vertices.size(); index++) {
      UnsignedInt indexA = index;
      UnsignedInt indexB = (index + 1) % 3;
      UnsignedInt indexC = (index + 2) % 3;
      if (vertexTypes[indexC] == VertexPosition::COPLANAR) {
        Vertex a = triangle.vertices[indexA];
        Vertex b = triangle.vertices[indexB];
        Vertex c = triangle.vertices[indexC];
        Line line(a.position, b.position);
        Vector3 intersection = Geometry::intersect(line, plane);
        Triangle triangleA(a.position, intersection, c.position);
        Triangle triangleB(b.position, c.position, intersection);
        if (vertexTypes[indexA] == VertexPosition::BACK) {
          back.emplace_back(triangleA);
          front.emplace_back(triangleB);
        } else {
          back.emplace_back(triangleB);
          front.emplace_back(triangleA);
        }
        return;
      }
    }
  }

  if (backVertexCount == 2 || frontVertexCount == 2) {
    if (coplanarVertexCount == 1) {
      if (backVertexCount == 2) {
        back.emplace_back(triangle);
      } else {
        front.emplace_back(triangle);
      }
    } else {
      for (UnsignedInt index = 0; index < triangle.vertices.size(); index++) {
        UnsignedInt indexA = index;
        UnsignedInt indexB = (index + 1) % 3;
        UnsignedInt indexC = (index + 2) % 3;
        if (vertexTypes[indexA] == vertexTypes[indexB]) {
          Vertex a = triangle.vertices[indexA];
          Vertex b = triangle.vertices[indexB];
          Vertex c = triangle.vertices[indexC];
          Line lineA(a.position, c.position);
          Line lineB(b.position, c.position);
          Vector3 intersectionA = Geometry::intersect(lineA, plane);
          Vector3 intersectionB = Geometry::intersect(lineB, plane);
          Triangle triangleA(a.position, b.position, intersectionA);
          Triangle triangleB(b.position, intersectionB, intersectionA);
          Triangle triangleC(intersectionB, c.position, intersectionA);
          if (vertexTypes[indexA] == VertexPosition::BACK &&
              vertexTypes[indexB] == VertexPosition::BACK) {
            back.emplace_back(triangleA);
            back.emplace_back(triangleB);
            front.emplace_back(triangleC);
          } else {
            back.emplace_back(triangleC);
            front.emplace_back(triangleA);
            front.emplace_back(triangleB);
          }
        }
      }
    }
    return;
  }

  throw std::logic_error("Couldn't split triangle.");
}

auto Geometry::translate(Triangle triangle, Vector3 translation) -> Triangle {
  for (Vertex &vertex : triangle.vertices) {
    vertex.position += translation;
  }
  return triangle;
}

auto Geometry::translate(std::vector<Triangle> triangles, Vector3 translation)
    -> std::vector<Triangle> {
  std::vector<Triangle> translatedTriangles;
  for (Triangle triangle : triangles) {
    Triangle translatedTriangle = Geometry::translate(triangle, translation);
    translatedTriangles.push_back(translatedTriangle);
  }
  return translatedTriangles;
}

#pragma once

#include <algorithm>
#include <exception>
#include <math.h>
#include <stdexcept>
#include <vector>

#include <Corrade/Containers/StaticArray.h>
#include <Corrade/Utility/Debug.h>
#include <Geometry/Line.h>
#include <Geometry/Plane.h>
#include <Geometry/Triangle.h>
#include <Geometry/Vertex.h>
#include <Magnum/Magnum.h>
#include <Magnum/Math/Complex.h>
#include <Magnum/Math/Matrix4.h>
#include <Magnum/Types.h>

namespace Geometry {
auto flip(Triangle triangle) -> Triangle;
auto flip(std::vector<Triangle> triangles) -> std::vector<Triangle>;
auto intersect(const Line &line, const Plane &plane) -> Vector3;
auto rotate(Triangle triangle, Deg angle, Vector3 axis) -> Triangle;
auto rotate(std::vector<Triangle> triangles, Deg angle, Vector3 axis)
    -> std::vector<Triangle>;
auto split(Plane &plane, Triangle &triangle, std::vector<Triangle> &back,
           std::vector<Triangle> &coplanarBack,
           std::vector<Triangle> &coplanarFront, std::vector<Triangle> &front)
    -> void;
auto translate(Triangle triangle, Vector3 translation) -> Triangle;
auto translate(std::vector<Triangle> triangles, Vector3 translation)
    -> std::vector<Triangle>;
} // namespace Geometry

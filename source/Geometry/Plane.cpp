#include <Geometry/Plane.h>

Plane::Plane() = default;

Plane::Plane(const Float distance, const Vector3 normal)
    : distance(distance), normal(normal) {}

Plane::Plane(const Triangle &triangle) {
  Vertex vertex = triangle.vertices.front();
  distance = Math::dot(vertex.position, vertex.normal);
  normal = triangle.normal();
}

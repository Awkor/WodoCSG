#include <Geometry/Shape.h>

auto Shape::arc(Vector3 a, Vector3 b, Float bulge, Float width, Float depth,
                UnsignedInt segmentCount) -> std::vector<Triangle> {
  Vector3 delta = a - b;
  Vector3 halfDelta = delta / 2;
  Deg angle = 4 * Math::atan(bulge);
  Float radius = halfDelta.length() / Math::sin(angle / 2);
  Deg segmentAngle = angle / segmentCount;
  Float halfDepth = depth / 2.0f;
  Float halfWidth = width / 2.0f;
  Float innerRadius = radius - halfWidth;
  Float outerRadius = radius + halfWidth;
  Vector3 xAxis = Vector3::xAxis();
  std::vector<Triangle> triangles;
  for (UnsignedInt segmentIndex = 0; segmentIndex < segmentCount;
       segmentIndex++) {
    UnsignedInt indexA = segmentIndex;
    UnsignedInt indexB = segmentIndex + 1;
    Deg angleA = segmentAngle * indexA;
    Deg angleB = segmentAngle * indexB;
    Float xA = Math::cos(angleA);
    Float yA = Math::sin(angleA);
    Float xB = Math::cos(angleB);
    Float yB = Math::sin(angleB);
    Vector3 bottomBackA(xA * outerRadius, yA * outerRadius, -halfDepth);
    Vector3 bottomBackB(xB * outerRadius, yB * outerRadius, -halfDepth);
    Vector3 bottomFrontA(xA * innerRadius, yA * innerRadius, -halfDepth);
    Vector3 bottomFrontB(xB * innerRadius, yB * innerRadius, -halfDepth);
    Vector3 topBackA(xA * outerRadius, yA * outerRadius, halfDepth);
    Vector3 topBackB(xB * outerRadius, yB * outerRadius, halfDepth);
    Vector3 topFrontA(xA * innerRadius, yA * innerRadius, halfDepth);
    Vector3 topFrontB(xB * innerRadius, yB * innerRadius, halfDepth);
    if (indexA == 0) {
      std::vector<Triangle> right =
          Shape::cylinder(180.0_degf, width / 2, depth, 8);
      right = Geometry::rotate(right, 90.0_degf, xAxis);
      Vector3 deltaA = bottomBackA - topFrontA;
      Vector3 middleA = bottomBackA - deltaA / 2;
      right = Geometry::translate(right, middleA);
      triangles.insert(triangles.end(), right.begin(), right.end());
    } else if (indexA == segmentCount - 1) {
      Vector3 deltaB = bottomBackB - topFrontB;
      Vector2 direction = (bottomBackB.xy() - topFrontB.xy()).normalized();
      Vector3 middleB = bottomBackB - deltaB / 2;
      Deg angle = Math::angle(direction, xAxis.xy());
      std::vector<Triangle> left =
          Shape::cylinder(180.0_degf, width / 2, depth, 8);
      left = Geometry::rotate(left, -90.0_degf, xAxis);
      Vector3 rotationAxis =
          deltaB.y() > 0 ? Vector3::zAxis() : -Vector3::zAxis();
      left = Geometry::rotate(left, angle, rotationAxis);
      left = Geometry::translate(left, middleB);
      triangles.insert(triangles.end(), left.begin(), left.end());
    }
    std::vector<Triangle> back =
        Shape::rectangle(bottomBackA, topBackA, bottomBackB, topBackB);
    std::vector<Triangle> bottom =
        Shape::rectangle(bottomBackB, bottomFrontB, bottomBackA, bottomFrontA);
    std::vector<Triangle> front =
        Shape::rectangle(bottomFrontB, topFrontB, bottomFrontA, topFrontA);
    std::vector<Triangle> top =
        Shape::rectangle(topFrontB, topBackB, topFrontA, topBackA);
    triangles.insert(triangles.end(), back.begin(), back.end());
    triangles.insert(triangles.end(), bottom.begin(), bottom.end());
    triangles.insert(triangles.end(), front.begin(), front.end());
    triangles.insert(triangles.end(), top.begin(), top.end());
  }
  {
    Vector3 a(radius, 0.0f, 0.0f);
    Utility::Debug() << "a: " << a;
    Utility::Debug() << "angle: " << angle;
    Vector3 b(Math::cos(angle) * radius, Math::sin(angle) * radius, 0.0f);
    Utility::Debug() << "b: " << b;
    Vector3 delta = a - b;
    Utility::Debug() << "delta: " << delta;
    Vector3 halfDelta = delta / 2;
    Vector3 middle = a - halfDelta;
    Utility::Debug() << "middle: " << middle;
    Vector2 perpendicular =
        delta.xy().normalized().perpendicular(); // TODO: Rename.
    Float sagitta = bulge * radius;
    Float length = radius - sagitta; // TODO: Rename.
    Vector2 offset = perpendicular * length;
    Vector3 center(middle.xy() - offset, 0.0f);
    Utility::Debug() << "center: " << center;
    triangles = Geometry::translate(triangles, -center);
  }
  {
    Float rotationX = Math::cos(angle) * radius;
    Float rotationY = Math::sin(angle) * radius;
    Vector2 vector(rotationX, rotationY);
    vector = vector - Vector2(radius, 0.0f);
    Deg rotationAngle =
        Math::angle(vector.normalized(), delta.xy().normalized());
    triangles = Geometry::rotate(triangles, rotationAngle, Vector3::zAxis());
  }
  {
    Vector2 perpendicular =
        delta.xy().normalized().perpendicular(); // TODO: Rename.
    Float sagitta = bulge * radius;
    Float length = radius - sagitta; // TODO: Rename.
    Vector2 offset = perpendicular * length;
    Float halfDeltaLength = delta.length() / 2;
    length = Math::sqrt(
        Math::abs(halfDeltaLength * halfDeltaLength - radius * radius));
    Vector3 middle = a - delta / 2;
    Vector3 translation(middle.xy() + offset, 0.0f);
    triangles = Geometry::translate(triangles, translation);
  }
  return triangles;
}

auto Shape::circle(Deg angle, Float radius, UnsignedInt sideCount)
    -> std::vector<Triangle> {
  Deg sideAngle = angle / sideCount;
  Vector3 center(0.0f);
  std::vector<Triangle> triangles;
  for (UnsignedInt sideIndex = 0; sideIndex < sideCount; sideIndex++) {
    UnsignedInt indexA = sideIndex;
    UnsignedInt indexB = sideIndex + 1;
    Deg angleA = sideAngle * indexA;
    Deg angleB = sideAngle * indexB;
    Float xA = Math::cos(angleA);
    Float yA = Math::sin(angleA);
    Float xB = Math::cos(angleB);
    Float yB = Math::sin(angleB);
    Vector3 postionA(xA * radius, yA * radius, 0.0f);
    Vector3 postionB(xB * radius, yB * radius, 0.0f);
    triangles.emplace_back(postionA, postionB, center);
  }
  return triangles;
}

auto Shape::cuboid(Float width, Float height, Float depth)
    -> std::vector<Triangle> {
  Float halfWidth = width / 2;
  Float halfHeight = height / 2;
  Float halfDepth = depth / 2;
  Vector3 leftBottomBack(-halfWidth, -halfHeight, -halfDepth);
  Vector3 leftBottomFront(-halfWidth, -halfHeight, halfDepth);
  Vector3 leftTopBack(-halfWidth, halfHeight, -halfDepth);
  Vector3 leftTopFront(-halfWidth, halfHeight, halfDepth);
  Vector3 rightBottomBack(halfWidth, -halfHeight, -halfDepth);
  Vector3 rightBottomFront(halfWidth, -halfHeight, halfDepth);
  Vector3 rightTopBack(halfWidth, halfHeight, -halfDepth);
  Vector3 rightTopFront(halfWidth, halfHeight, halfDepth);
  std::vector<Triangle> triangles(12);
  // Left face.
  triangles[0] = {leftTopBack, leftBottomFront, leftTopFront};
  triangles[1] = {leftTopBack, leftBottomBack, leftBottomFront};
  // Right face.
  triangles[2] = {rightTopFront, rightBottomBack, rightTopBack};
  triangles[3] = {rightTopFront, rightBottomFront, rightBottomBack};
  // Top face.
  triangles[4] = {leftTopBack, rightTopFront, rightTopBack};
  triangles[5] = {leftTopBack, leftTopFront, rightTopFront};
  // Bottom face.
  triangles[6] = {leftBottomFront, rightBottomBack, rightBottomFront};
  triangles[7] = {leftBottomFront, leftBottomBack, rightBottomBack};
  // Front face.
  triangles[8] = {leftTopFront, rightBottomFront, rightTopFront};
  triangles[9] = {leftTopFront, leftBottomFront, rightBottomFront};
  // Back face.
  triangles[10] = {rightTopBack, leftBottomBack, leftTopBack};
  triangles[11] = {rightTopBack, rightBottomBack, leftBottomBack};
  return triangles;
}

auto Shape::cuboid(Vector3 dimensions) -> std::vector<Triangle> {
  return Shape::cuboid(dimensions.x(), dimensions.y(), dimensions.z());
}

auto Shape::cylinder(Deg angle, Float radius, Float height,
                     UnsignedInt sideCount) -> std::vector<Triangle> {
  // Create and position the bottom and top parts of the cylinder.
  std::vector<Triangle> bottom = circle(angle, radius, sideCount);
  std::vector<Triangle> top = circle(angle, radius, sideCount);
  Vector3 yAxis = Vector3::yAxis();
  bottom = Geometry::rotate(bottom, 180.0_degf, yAxis);
  Float halfHeight = height / 2;
  Vector3 bottomTranslation = Vector3(0.0f, 0.0f, -halfHeight);
  Vector3 topTranslation = Vector3(0.0f, 0.0f, halfHeight);
  bottom = Geometry::translate(bottom, bottomTranslation);
  top = Geometry::translate(top, topTranslation);
  // Create and position the side parts of the cylinder.
  Deg sideAngle = angle / sideCount;
  std::vector<Triangle> triangles;
  for (UnsignedInt sideIndex = 0; sideIndex < sideCount; sideIndex++) {
    UnsignedInt indexA = sideIndex;
    UnsignedInt indexB = sideIndex + 1;
    Deg angleA = sideAngle * indexA;
    Deg angleB = sideAngle * indexB;
    Float xA = Math::cos(angleA) * radius;
    Float yA = Math::sin(angleA) * radius;
    Float xB = Math::cos(angleB) * radius;
    Float yB = Math::sin(angleB) * radius;
    Vector3 leftBottom = Vector3(xA, yA, -halfHeight);
    Vector3 leftTop = Vector3(xA, yA, halfHeight);
    Vector3 rightBottom = Vector3(xB, yB, -halfHeight);
    Vector3 rightTop = Vector3(xB, yB, halfHeight);
    triangles.emplace_back(leftTop, rightBottom, rightTop);
    triangles.emplace_back(leftTop, leftBottom, rightBottom);
  }
  triangles.insert(triangles.end(), top.begin(), top.end());
  triangles.insert(triangles.end(), bottom.begin(), bottom.end());
  Vector3 xAxis = Vector3::xAxis();
  triangles = Geometry::rotate(triangles, 90.0_degf, xAxis);
  return triangles;
}

auto Shape::rectangle(Vector3 leftBottom, Vector3 leftTop, Vector3 rightBottom,
                      Vector3 rightTop) -> std::vector<Triangle> {
  std::vector<Triangle> triangles(2);
  triangles[0] = {leftTop, rightBottom, rightTop};
  triangles[1] = {leftTop, leftBottom, rightBottom};
  return triangles;
}

auto Shape::rectangle(Float width, Float height) -> std::vector<Triangle> {
  Float halfHeight = height / 2;
  Float halfWidth = width / 2;
  Vector3 leftBottom = Vector3(-halfWidth, -halfHeight, 0.0f);
  Vector3 leftTop = Vector3(-halfWidth, halfHeight, 0.0f);
  Vector3 rightBottom = Vector3(halfWidth, -halfHeight, 0.0f);
  Vector3 rightTop = Vector3(halfWidth, halfHeight, 0.0f);
  return Shape::rectangle(leftBottom, leftTop, rightBottom, rightTop);
}

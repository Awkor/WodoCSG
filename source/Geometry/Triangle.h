#pragma once

#include <vector>

#include <Geometry/Vertex.h>
#include <Magnum/Magnum.h>
#include <Magnum/Math/Vector3.h>

using namespace Magnum;

struct Triangle {
  std::vector<Vertex> vertices;
  Triangle();
  Triangle(const Vector3 a, const Vector3 b, const Vector3 c);
  auto normal() const -> Vector3;
};

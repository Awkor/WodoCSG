#include <Geometry/Line.h>

Line::Line(const Vertex a, const Vertex b) : vertices{a, b} {}

auto Line::direction(const bool inverted) const -> Vector3 {
  Vertex a = vertices.at(0);
  Vertex b = vertices.at(1);
  Vector3 delta = inverted ? a.position - b.position : b.position - a.position;
  return delta.normalized();
}

#include <Geometry/Triangle.h>

Triangle::Triangle() = default;

Triangle::Triangle(const Vector3 a, const Vector3 b, const Vector3 c)
    : vertices{a, b, c} {
  Vector3 normal = this->normal();
  for (Vertex &vertex : vertices) {
    vertex.normal = normal;
  }
}

auto Triangle::normal() const -> Vector3 {
  Vertex a = vertices[0];
  Vertex b = vertices[1];
  Vertex c = vertices[2];
  Vector3 d = b.position - a.position;
  Vector3 e = c.position - a.position;
  Vector3 normal = Math::cross(d, e);
  return normal.normalized();
}

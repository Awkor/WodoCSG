#include <malloc.h>
#include <vector>

#include <Bsp/BspTreeNode.h>
#include <Corrade/Containers/Array.h>
#include <Corrade/Containers/ArrayViewStl.h>
#include <Corrade/Containers/Containers.h>
#include <Corrade/Containers/GrowableArray.h>
#include <Corrade/Containers/Pointer.h>
#include <Corrade/Containers/Reference.h>
#include <Corrade/Containers/StridedArrayView.h>
#include <Corrade/Utility/Debug.h>
#include <Corrade/Utility/DebugStl.h>
#include <Corrade/Utility/FormatStl.h>
#include <Csg/Csg.h>
#include <Geometry/Geometry.h>
#include <Geometry/Shape.h>
#include <Geometry/Triangle.h>
#include <Geometry/Vertex.h>
#include <Magnum/GL/Buffer.h>
#include <Magnum/GL/DefaultFramebuffer.h>
#include <Magnum/GL/GL.h>
#include <Magnum/GL/Mesh.h>
#include <Magnum/GL/PrimitiveQuery.h>
#include <Magnum/GL/Renderer.h>
#include <Magnum/GL/Version.h>
#include <Magnum/Magnum.h>
#include <Magnum/Math/Color.h>
#include <Magnum/Math/Constants.h>
#include <Magnum/Math/Matrix4.h>
#include <Magnum/MeshTools/Interleave.h>
#include <Magnum/Platform/GLContext.h>
#include <Magnum/Platform/GlfwApplication.h>
#include <Magnum/Primitives/Cube.h>
#include <Magnum/Shaders/MeshVisualizer.h>
#include <Magnum/Shaders/Phong.h>
#include <Magnum/Shaders/VertexColor.h>
#include <Magnum/Tags.h>
#include <Magnum/Trade/MeshData.h>
#include <Magnum/Trade/Trade.h>
#include <Magnum/Types.h>

using namespace Magnum;
using namespace Magnum::Math::Literals;

enum class PanelFace { BACK, BOTTOM, FRONT, LEFT, RIGHT, TOP };

class WodoCSG : public Platform::Application {
public:
  explicit WodoCSG(const Arguments &arguments);

private:
  Float mouseSensitivity;
  Float scrollSensitivity;
  Color3 panelDiffuseColor;
  Vector3 panelDimensions;
  GL::Mesh panelMesh{NoCreate};
  Shaders::PhongGL panelShader{NoCreate};
  Color3 panelAmbientColor;
  Matrix4 projection;
  Matrix4 transformation;
  Vector4 lightPosition;
  Color3 subtractionAmbientColor;
  Color3 subtractionDiffuseColor;
  Containers::Array<Containers::Pointer<GL::Mesh>> subtractionMeshes;
  Shaders::PhongGL subtractionShader{NoCreate};
  std::vector<std::vector<Triangle>> subtractions;

  void addHole(PanelFace face, Vector3 position, Float diameter, Float depth);
  void drawEvent() override;
  void mouseMoveEvent(MouseMoveEvent &event) override;
  void mouseScrollEvent(MouseScrollEvent &event) override;
};

WodoCSG::WodoCSG(const Arguments &arguments)
    : Platform::Application{arguments, NoCreate} {
  // We need to create the OpenGL context manually to be able to specify the
  // multisampling sample count.
  {
    Configuration configuration;
    configuration.setTitle("WodoCSG");
    configuration.setSize({640, 360});
    GLConfiguration glConfiguration;
    glConfiguration.setVersion(GL::version(3, 2));
    Int multisamplingSampleCount = 4;
    glConfiguration.setSampleCount(multisamplingSampleCount);
    bool success = tryCreate(configuration, glConfiguration);
    if (success == false) {
      glConfiguration.setSampleCount(0);
      create(configuration, glConfiguration);
    }
  }

  lightPosition = {1.0f, 1.0f, 1.0f, 0.0f};
  mouseSensitivity = 5.0f;
  panelAmbientColor = 0x364fc7_rgbf;
  panelDiffuseColor = 0x364fc7_rgbf;
  panelDimensions = Vector3(100.0f, 3.0f, 50.0f);
  scrollSensitivity = 5.0f;
  subtractionAmbientColor = 0x4dabf7_rgbf;
  subtractionDiffuseColor = 0x4dabf7_rgbf;

  addHole(PanelFace::FRONT, Vector3(40.0f, 1.5f, 0.0f), 1.5f, 50.0f);
  addHole(PanelFace::FRONT, Vector3(50.0f, 1.5f, 0.0f), 1.5f, 50.0f);
  addHole(PanelFace::FRONT, Vector3(45.0f, 1.5f, 0.0f), 1.5f, 50.0f);
  addHole(PanelFace::FRONT, Vector3(60.0f, 1.5f, 0.0f), 1.5f, 50.0f);
  addHole(PanelFace::FRONT, Vector3(55.0f, 1.5f, 0.0f), 1.5f, 50.0f);

  GL::Renderer::enable(GL::Renderer::Feature::DepthTest);
  GL::Renderer::enable(GL::Renderer::Feature::FaceCulling);

  {
    Vector3 a = Vector3(-30.0f, 0.0f, 0.0f);
    Vector3 b = Vector3(30.0f, 0.0f, 0.0f);
    std::vector<Triangle> panel = Shape::cuboid(80.0f, 40.0f, 5.0f);
    for (std::vector<Triangle> &subtraction : subtractions) {
      // panel = Csg::subtract(panel, subtraction);
    }

    // std::vector<Triangle> cubeA = Shape::cuboid(100.0f, 2.0f, 5.0f);
    // std::vector<Triangle> cubeB = Shape::cuboid(2.0f, 20.0f, 50.0f);
    // std::vector<Triangle> cubeC = Csg::merge(cubeA, cubeB);

    std::vector<Triangle> arc = Shape::arc(a, b, 0.2f, 8.0f, 8.0f, 16);
    panel = Csg::subtract(panel, arc);

    {
      Vector3 a = Vector3(-10.0f, 0.0f, 0.0f);
      Vector3 b = Vector3(10.0f, 0.0f, 0.0f);
      Vector3 c = Vector3(20.0f, 0.0f, 0.0f);
      Vector3 d = Vector3(30.0f, 0.0f, 0.0f);
      Float bulge = 0.5f;
      std::vector<Triangle> arcA = Shape::arc(a, b, bulge, 1.0f, 1.0f, 8);
      std::vector<Triangle> arcB = Shape::arc(b, c, -bulge, 1.0f, 1.0f, 8);
      std::vector<Triangle> arcC = Shape::arc(c, d, bulge, 1.0f, 1.0f, 8);
      arcA = Geometry::rotate(arcA, 90.0_degf, Vector3::xAxis());
      arcA = Geometry::translate(arcA, {0.0f, 1.0f, 0.0f});
      arcB = Geometry::rotate(arcB, 90.0_degf, Vector3::xAxis());
      arcB = Geometry::translate(arcB, {0.0f, 1.0f, 0.0f});
      arcC = Geometry::rotate(arcC, 90.0_degf, Vector3::xAxis());
      arcC = Geometry::translate(arcC, {0.0f, 1.0f, 0.0f});
      // panel = Csg::subtract(panel, arcA);
      // panel = Csg::subtract(panel, arcB);
      // panel = Csg::subtract(panel, arcC);
    }

    Containers::Array<Vertex> vertices;
    for (Triangle &triangle : panel) {
      for (Vertex vertex : triangle.vertices) {
        Containers::arrayAppend(vertices, vertex);
      }
    }

    Int vertexCount = vertices.size();

    UnsignedLong vertexSize = sizeof(Vertex);
    Containers::StridedArrayView1D<Vector3> vertexNormals =
        Containers::stridedArrayView(vertices, &vertices[0].normal, vertexCount,
                                     vertexSize);
    Containers::StridedArrayView1D<Vector3> vertexPositions =
        Containers::stridedArrayView(vertices, &vertices[0].position,
                                     vertexCount, vertexSize);

    Trade::MeshAttributeData attributes[]{
        Trade::MeshAttributeData{Trade::MeshAttribute::Normal, vertexNormals},
        Trade::MeshAttributeData{Trade::MeshAttribute::Position,
                                 vertexPositions}};

    Trade::MeshData meshData(
        MeshPrimitive::Triangles, {}, vertices,
        Trade::meshAttributeDataNonOwningArray(attributes));

    GL::Buffer vertexBuffer;
    vertexBuffer.setData(MeshTools::interleave(meshData.normalsAsArray(),
                                               meshData.positions3DAsArray()),
                         GL::BufferUsage::StaticDraw);

    panelMesh = GL::Mesh();
    panelMesh
        .addVertexBuffer(std::move(vertexBuffer), 0, Shaders::PhongGL::Normal{},
                         Shaders::PhongGL::Position{})
        .setPrimitive(GL::MeshPrimitive::Triangles)
        .setCount(vertexCount);
  }
  // Setup the transformation matrix.
  {
    Matrix4 startingRotationY = Matrix4::rotationX(15.0_degf);
    Matrix4 startingRotationX = Matrix4::rotationY(30.0_degf);
    transformation = startingRotationY * startingRotationX;
  }
  // Setup the projection matrix.
  {
    Deg cameraFieldOfView = 45.0_degf;
    Float windowAspectRation = Vector2{windowSize()}.aspectRatio();
    projection = Matrix4::perspectiveProjection(
        cameraFieldOfView, windowAspectRation, 0.01f, 1000.0f);
    Float cameraDistance = -150.0f;
    Vector3 cameraTranslation = Vector3::zAxis(cameraDistance);
    projection = projection * Matrix4::translation(cameraTranslation);
  }
  // Initialize the shaders.
  panelShader = Shaders::PhongGL();
  subtractionShader = Shaders::PhongGL();
}

void WodoCSG::drawEvent() {
  GL::PrimitiveQuery primitiveQuery(
      GL::PrimitiveQuery::Target::PrimitivesGenerated);
  primitiveQuery.begin();

  GL::defaultFramebuffer.clear(GL::FramebufferClear::Color |
                               GL::FramebufferClear::Depth);

  GL::Renderer::enable(GL::Renderer::Feature::FaceCulling);

  panelShader.setAmbientColor(panelAmbientColor)
      .setDiffuseColor(panelDiffuseColor)
      .setLightPositions({lightPosition})
      .setNormalMatrix(transformation.normalMatrix())
      .setProjectionMatrix(projection)
      .setTransformationMatrix(transformation)
      .draw(panelMesh);

  GL::defaultFramebuffer.clear(GL::FramebufferClear::Depth);

  GL::Renderer::disable(GL::Renderer::Feature::FaceCulling);

  for (Containers::Pointer<GL::Mesh> &operationMesh : subtractionMeshes) {
    GL::Mesh &mesh = *operationMesh.get();
    subtractionShader.setAmbientColor(subtractionAmbientColor)
        .setDiffuseColor(subtractionDiffuseColor)
        .setLightPositions({lightPosition})
        .setNormalMatrix(transformation.normalMatrix())
        .setProjectionMatrix(projection)
        .setTransformationMatrix(transformation)
        .draw(mesh);
  }

  swapBuffers();

  primitiveQuery.end();
  UnsignedInt primitiveCount = primitiveQuery.result<UnsignedInt>();
  // Utility::Debug() << "primitiveCount:" << primitiveCount;
}

void WodoCSG::mouseMoveEvent(MouseMoveEvent &event) {
  if (event.buttons() & MouseMoveEvent::Button::Left) {
    Vector2 delta = Vector2(event.relativePosition());
    delta = delta / Vector2(windowSize());
    delta = delta * mouseSensitivity;
    Matrix4 horizontalRotation = Matrix4::rotationY(Rad{delta.x()});
    Matrix4 verticalRotation = Matrix4::rotationX(Rad{delta.y()});
    transformation = verticalRotation * transformation * horizontalRotation;
    event.setAccepted();
    redraw();
  }
}

void WodoCSG::mouseScrollEvent(MouseScrollEvent &event) {
  Float offset = event.offset().y() * scrollSensitivity;
  Vector3 translation = Vector3::zAxis(offset);
  projection = projection * Matrix4::translation(translation);
  event.setAccepted();
  redraw();
}

// TODO: Add missing translations.
void WodoCSG::addHole(PanelFace face, Vector3 position, Float diameter,
                      Float depth) {
  Float radius = diameter / 2;
  std::vector<Triangle> hole = Shape::cylinder(360.0_degf, radius, depth, 20);
  Float halfPanelWidth = panelDimensions.x() / 2.0f;
  Float halfPanelHeight = panelDimensions.y() / 2.0f;
  Float halfPanelDepth = panelDimensions.z() / 2.0f;
  Vector3 translation =
      Vector3(-halfPanelWidth, -halfPanelHeight, halfPanelDepth);
  switch (face) {
  case PanelFace::BACK:
    hole = Geometry::rotate(hole, -90.0_degf, Vector3::xAxis());
    break;
  case PanelFace::BOTTOM:
    break;
  case PanelFace::FRONT:
    hole = Geometry::rotate(hole, 90.0_degf, Vector3::xAxis());
    translation += Vector3(0.0f, 0.0f, -depth / 2);
    break;
  case PanelFace::LEFT:
    hole = Geometry::rotate(hole, 90.0_degf, Vector3::zAxis());
    break;
  case PanelFace::RIGHT:
    hole = Geometry::rotate(hole, -90.0_degf, Vector3::zAxis());
    break;
  case PanelFace::TOP:
    break;
  }
  hole = Geometry::translate(hole, translation);
  hole = Geometry::translate(hole, position);
  subtractions.emplace_back(hole);
}

MAGNUM_APPLICATION_MAIN(WodoCSG)

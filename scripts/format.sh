#!/bin/sh

clang-format -i \
./source/Bsp/*.h \
./source/Bsp/*.cpp \
./source/Csg/*.h \
./source/Csg/*.cpp \
./source/Geometry/*.h \
./source/Geometry/*.cpp \
./source/WodoCSG.cpp && \
cmake-format -i \
CMakeLists.txt \
./source/CMakeLists.txt
